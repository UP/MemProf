
/*---------------------------------------------------------------------------*/
/*--- MemProf: Valgrind tool memory allocation profiling        mp_main.c ---*/
/*---------------------------------------------------------------------------*/

/*
   This file is part of MemProf, a heavyweight Valgrind tool for
   memory usage profiling.

   Copyright (C) 2016 Cristina-Gabriela Moraru @ CERN
      cristina.moraru09@gmail.com

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#include "pub_tool_basics.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_hashtable.h"
#include "pub_tool_poolalloc.h"
#include "pub_tool_mallocfree.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_libcfile.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_options.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_aspacemgr.h"

#include "mp_include.h"

void* extended_buffer = NULL;
UInt ext_buffer_sz = 64000;
UInt ext_buffer_index = 0;

UInt stacktrace_depth = 30;
SizeT count_records = 0;

UInt safe_create_logfile(HChar* name) {
   SysRes sres;
   UInt fd;

   sres = VG_(open)(name, 
                    VKI_O_CREAT|VKI_O_RDWR|VKI_O_TRUNC, 
                    VKI_S_IRUSR|VKI_S_IWUSR|VKI_S_IRGRP|VKI_S_IROTH);
   if (!sr_isError(sres)) {
      fd = sr_Res(sres);
   } else {
      VG_(message)(Vg_UserMsg, "ERROR: Can't create log file '%s'\n", name);
      VG_(message_flush)();
      VG_(exit)(1);
      /*NOTREACHED*/
   }
   return fd;
}


static Bool mp_process_cmd_line_options(const HChar* arg)
{
  if (VG_INT_CLO(arg, "--uncompressed-buf", ext_buffer_sz)) {
    return True;
  }
  else if (VG_INT_CLO(arg, "--stacktrace-depth", stacktrace_depth)) {
    return True;
  }
  else if VG_STR_CLO(arg, "--log-basename", base_log_name)
  {
    return True;
  }
  return False;
}

static void mp_print_usage(void)
{
   VG_(printf)(
"    --uncompressed-buf=<number>          size of the uncompressed output buffer [64000]\n"
"    --stacktrace-depth=<number>          depth of the captured stacktrace [30]\n"
"    --log-basename=<string>         basename of statistics logfile name and hash logfile name"
                                                      "[<PID>-stat.log / <PID>-hash.log]\n"
   );
}

static void mp_print_debug_usage(void)
{  
   VG_(printf)(
"    (none)\n"
   );
}

void create_logfile_names() {
  if (base_log_name) {
    VG_(sprintf)(stat_log_name, "%s-stat.log", base_log_name);
    VG_(sprintf)(hash_log_name, "%s-hash.log", base_log_name);
  }
  else {
    VG_(sprintf)(stat_log_name, "%d-stat.log", VG_(getpid)());
    VG_(sprintf)(hash_log_name, "%d-hash.log", VG_(getpid)()); 
  }
}

static void mp_post_clo_init(void)
{
  MP_(ips_poolalloc) = VG_(newPA)
      (sizeof(InstrPointerExtra),
       1000,
       VG_(malloc),
       "sc.cSC.2 (InstrPointerExtra pools)",
       VG_(free));
  MP_(sizes_poolalloc) = VG_(newPA)
      (sizeof(SizeExtra),
       1000,
       VG_(malloc),
       "sc.cSC.2 (SizeExtra pools)",
       VG_(free));

  create_logfile_names();
  stat_log_fd = safe_create_logfile(stat_log_name);
  hash_log_fd = safe_create_logfile(hash_log_name);

  create_file_header();

 ext_buffer_index += StatHeader.HeaderSize;
}

static
IRSB* mp_instrument ( VgCallbackClosure* closure,
                      IRSB* bb,
                      const VexGuestLayout* layout, 
                      const VexGuestExtents* vge,
                      const VexArchInfo* archinfo_host,
                      IRType gWordTy, IRType hWordTy )
{
    return bb;
}

static void mp_fini(Int exitcode)
{
  if (ext_buffer_index > 0) {
    MP_(flush_buffer)();
  }

  dump_stacktrace_extra_info();

  VG_(lseek)(stat_log_fd, 0, VKI_SEEK_SET);
  VG_(write)(stat_log_fd, (HChar*)&StatHeader,
    StatHeader.HeaderSize - StatHeader.CmdLength);
  VG_(write)(stat_log_fd, StatHeader.CmdLine, StatHeader.CmdLength);

  VG_(close)(stat_log_fd);
  VG_(close)(hash_log_fd);
}

static void mp_pre_clo_init(void)
{
  VG_(details_name)            ("MemProf");
  VG_(details_version)         (NULL);
  VG_(details_description)     ("Memory Allocation Profiling Valgrind tool");
  VG_(details_copyright_author)(
  "Copyright (C) 2016, and GNU GPL'd, by Cristina-Gabriela Moraru @ CERN");
  VG_(details_bug_reports_to)  (VG_BUGS_TO);

  VG_(details_avg_translation_sizeB) ( 275 );

  VG_(basic_tool_funcs)        (mp_post_clo_init,
                                mp_instrument,
                                mp_fini);
  VG_(needs_command_line_options)(mp_process_cmd_line_options,
                                   mp_print_usage,
                                   mp_print_debug_usage);
  VG_(needs_malloc_replacement)  (MP_(malloc),
                                  MP_(__builtin_new),
                                  MP_(__builtin_vec_new),
                                  MP_(memalign),
                                  MP_(calloc),
                                  MP_(free),
                                  MP_(__builtin_delete),
                                  MP_(__builtin_vec_delete),
                                  MP_(realloc),
                                  MP_(malloc_usable_size), 
                                  MP_MALLOC_DEFAULT_REDZONE_SZB );

  tl_assert(MP_(ips_poolalloc) == NULL);
  tl_assert(MP_(sizes_poolalloc) == NULL);

  MP_(stacktrace_extra_info) = VG_(HT_construct)( "MP_(stacktrace_extra_info)" );
  extended_buffer = VG_(malloc) ("extended_buffer", ext_buffer_sz * sizeof(HChar));
  VG_(memset)(extended_buffer, 0, ext_buffer_sz);

  MP_(sizes_hash) = VG_(HT_construct)( "MP_(sizes_hash)" );
}

VG_DETERMINE_INTERFACE_VERSION(mp_pre_clo_init)

/*--------------------------------------------------------------------*/
/*--- end                                                          ---*/
/*--------------------------------------------------------------------*/
